# Charts
Repository for ABA charts

## Installation

```shell
$ brew install kubernetes-helm
$ helm repo add aba https://abaenglish.github.io/charts
```

If you want to use charts in a new cluster make sure you have installed tiller on the cluster with this command: ```helm init```

## Usage

To install a release train in your minikube you can execute:
```shell
$ helm install aba/AbaPlatform --version Archimedes.SR1 -n aba-platform --set api-gateway.service.hostPort=80,websockets-to-mq.service.hostPort=60000
```

## How it works
* [Helm](https://github.com/kubernetes/helm) - Helm is a tool for managing Kubernetes charts
* [Charts](https://github.com/kubernetes/charts) - Charts are packages of pre-configured Kubernetes resources

If you want to create a new chart, these are the instructions:
```shell
$ helm create newchart
$ helm package newchart
$ mv newchart-0.1.0.tgz docs
$ helm repo index docs --url https://abaenglish.github.io/charts
$ git add -i
$ git commit -av
$ git push
```

After every push with services or rleasetrains changes will perform a job execution creating charts. If you want to do it manually:
```shell
$ helm package newchart
$ mv newchart-0.1.0.tgz docs
$ helm repo index docs --url https://abaenglish.github.io/charts
$ git add -i
$ git commit -av
$ git push
```


